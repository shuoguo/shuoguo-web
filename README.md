这是[硕果网](http://shuoguo.net)介绍 web 相关技术用的小项目，里面有些基本的资源，目的是准备必要而且最简单的工具，立即可以去方便的学习。

1 → 克隆仓库到本地
```
git clone https://gitlab.com/shuoguo/shuoguo-web.git
```
2 → 安装项目需要的东西
```
cd shuoguo-web
npm install
```
3 → 运行开发服务器
```
npm run watch
```
4 → 在浏览器里打开
```
http://localhost:3000
```
[shuoguo.net](http://shuoguo.net)
